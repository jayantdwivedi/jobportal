import React, { useState, useEffect } from "react";
import axios from "axios";
import { getToken } from "../../util/comman";
import "./Style.scss"

import JobPostCard from "../../components/UI/JobPostCard/JobPostCard";
import Pagination from "../UI/Pagination/Pagination";
import Emptystate from "../UI/EmptyState/Emptystate";
import Breadcrumb from "../UI/Breadcrumb/Breadcrumb";
import { Spinner } from "react-bootstrap";

export default function Recruiter() {
  const [jobs, setJobs] = useState([]);
  const [loading,setloading] = useState(true);

  const baseurl = process.env.REACT_APP_BASE_URL;
  const url = `${baseurl}/recruiters/jobs`;

  useEffect(() => {
    axios
      .get(url, {
        headers: {
          Authorization: getToken(),
        },
      })
      .then((res) => {
        // console.log(res);
        setJobs(res.data?.data?.data);
        setloading(false);
      });
  }, []);

  // handle click event of recruiter
  const clicked = () => {
    alert("View");
  };

  // Pagination
  const [pageNumber, setPageNumber] = useState(0);
  const jobsPerPage = 6;
  const pagesVisited = pageNumber * jobsPerPage;

  const pageCount = Math.ceil(jobs?.length / jobsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  const displayJobs = jobs
    ?.slice(pagesVisited, pagesVisited + jobsPerPage)
    .map(function (element) {
      return (
        <JobPostCard
          id={element.id}
          title={element.title}
          description={element.description}
          location={element.location}
          bntViewparticipant={true}
        />
      );
    });
  console.log(jobs?.length, "Job length");
  return (
    <div>
      
      {loading? 
      <div className="d-flex justify-content-center align-items-center spinner-body">
          <Spinner animation="grow" variant="danger" />
          <Spinner animation="grow" variant="info" />
          <Spinner animation="grow" variant="warning" />
          <Spinner animation="grow" variant="dark" />
      </div>
      : (
      <>
        <h4 className="text-light ms-4 job-posted-heading">Job Posted by you</h4>
        <div>
        {jobs?.length === 0 || jobs?.length === undefined ? (
          <Emptystate message={"Your Posted jobs are shown here"} rec={true} />
        ) : (
          <>
            <div className="d-flex flex-wrap">{displayJobs}</div>
            <div className="d-flex justify-content-center space-bottom">
              <Pagination pageCount={pageCount} onPageChange={changePage} />
            </div>
          </>
        )}
        </div>
       </>
       )}
      </div>
  );
}
