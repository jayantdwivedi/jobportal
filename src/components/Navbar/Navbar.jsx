import React, { useEffect, useState } from "react";
import "./navbar.scss";
import { Link } from "react-router-dom";
import { isLoggedin } from "../../util/comman";

import Login from "../UI/Login/Login";
import Logout from "../UI/Logout/Logout";

export default function Index({ isLogin, setIslogin }) {
  useEffect(() => {
    setIslogin(isLoggedin());
  }, []);
  return (
    <>
      <nav>
        <div className="d-flex justify-content-between">
          <Link to="/" className="navbar-brand brand me-auto">
            My<span className="light-blue">Jobs</span>
          </Link>
          {isLogin ? <Logout setIslogin={setIslogin} /> : <Login />}
        </div>
        <hr className=" bg-light w-80 m-auto primary" />
      </nav>
    </>
  );
}
