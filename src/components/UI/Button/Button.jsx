import React from "react";
import "../../../styles/form.scss";

export default function Button({
  name,
  onClick,
  btntype = "btn-primary",
  icon = "",
}) {
  return (
    <div className="text-center">
      <button
        className={`btn ${btntype} px-4`}
        data-bs-toggle="modal"
        data-bs-target="#exampleModal"
        onClick={onClick}
      >
        {icon}
        {name}
      </button>
    </div>
  );
}
