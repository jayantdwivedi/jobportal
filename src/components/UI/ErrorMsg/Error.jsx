import React from "react";
import "../../../styles/form.scss";

export default function Error({ errmsg }) {
  return <p className="error">{errmsg}</p>;
}
