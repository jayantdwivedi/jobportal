import React from "react";
import "../../../styles/form.scss";

export default function Input({
  onChange = () => {},
  labelText = "",
  placeholder,
  type = "text",
  addedClass = "",
  value,
}) {
  return (
    <div>
      <label>{labelText}</label>
      {labelText != "" ? <sup>*</sup> : ""}
      <input
        className={`input-box p-2 my-3 rounded ${addedClass}`}
        value={value}
        type={type}
        autoComplete="off"
        placeholder={placeholder}
        onChange={(e) => {
          onChange(e);
        }}
      />
    </div>
  );
}
