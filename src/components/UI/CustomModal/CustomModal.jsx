import React from "react";
import { Modal, closeButton } from "react-bootstrap";
import ModalCards from "../ModalCards/ModalCards";
import EmptyModalState from "../EmptyModalState/EmptyModalState";

export default function CustomModal({ show, setShow, participants }) {
  const handleClose = () => setShow(false);
  return (
    <Modal size="lg" show={show} onHide={handleClose} className="w-40">
      <Modal.Header closeButton>
        <Modal.Title>View Participants</Modal.Title>
      </Modal.Header>
      <p className="ms-3 p-0 mb-0">
        Total {participants?.length || 0} participants
      </p>
      <hr />
      {participants?.length > 0 ? (
        <Modal.Body className="d-flex justify-content-around bg-secondary m-2 rounded">
          {participants?.map((ele) => {
            return (
              <ModalCards
                name={ele.name}
                email={ele.email}
                skill={ele.skills}
              />
            );
          })}
        </Modal.Body>
      ) : (
        <Modal.Body className="d-flex bg-secondary justify-content-center align-items-center m-2 rounded">
          <EmptyModalState />
        </Modal.Body>
      )}

      <Modal.Footer></Modal.Footer>
    </Modal>
  );
}
