import React from "react";
import ReactPaginate from "react-paginate";
import "./pagination.scss";
import { FaArrowCircleRight, FaArrowCircleLeft } from "react-icons/fa";

export default function Pagination({ pageCount, onPageChange }) {
  return (
    <ReactPaginate
      previousLabel={<FaArrowCircleLeft />}
      nextLabel={<FaArrowCircleRight />}
      pageCount={pageCount}
      onPageChange={onPageChange}
      containerClassName={"paginationBttns"}
      previousLinkClassName={"previousBttn"}
      nextLinkClassName={"nextBttn"}
      disabledClassName={"paginationDisabled"}
      activeClassName={"paginationActive"}
    />
  );
}
