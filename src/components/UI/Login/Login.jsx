import React from "react";
import { Link } from "react-router-dom";
import "./LoginLogout.scss";

import Button from "../Button/Button";

export default function Login() {
  return (
    <>
      <Link to="/login" className="login-btn">
        <Button name="Login/Signup" />
      </Link>
    </>
  );
}
