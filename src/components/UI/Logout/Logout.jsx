import React, { useState } from "react";
import "../Login/LoginLogout.scss";
import { Link } from "react-router-dom";
import { getUserRole } from "../../../util/comman";

import { getUsername } from "../../../util/comman";
import Dropdown from "../Dropdown/Dropdown";

export default function Logout({ setIslogin }) {
  return (
    <>
      <div className="d-flex">
        {getUserRole() == 0 ? (
          <Link
            to="/dashboard/jobpost"
            className="d-inline-block login-btn me-4 mt-2"
          >
            Post a Job
          </Link>
        ) : (
          <Link
            to="/dashboard/appliedjobs"
            className="d-inline-block login-btn me-4 mt-2"
          >
            Applied Jobs
          </Link>
        )}

        <div className="bg-light text-dark login-btn d-flex justify-content-center align-items-center userFirstName ">
          {getUsername().charAt(0).toUpperCase()}
        </div>
        <Dropdown setIslogin={setIslogin} />
      </div>
    </>
  );
}
