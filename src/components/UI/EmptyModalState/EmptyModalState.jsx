import React from "react";
import { MdEventNote } from "react-icons/md";

export default function EmptyModalState() {
  return (
    <div>
      <h4>No Participants to show</h4>
      <div className="m-5 p-5 d-flex text-light text-large justify-content-center align-items-center">
        <p className="display-4">
          <MdEventNote />
        </p>
      </div>
    </div>
  );
}
