import React, { useState } from "react";
import "./modalcards.scss";

export default function CustomModal({ name, email, skill }) {
  const capitalonechar = name[0].toUpperCase();
  return (
    <div className="bg-light rounded participant-card border border-danger ms-3">
      <div className="d-flex">
        <div className="name">{capitalonechar}</div>
        <div className="ps-3">
          <p className="m-0">{name}</p>
          <p className="m-0 opacity-75">{email}</p>
        </div>
      </div>
      <p className="mt-2 mb-0">Skills</p>
      <p className="m-0 opacity-75">{skill}</p>
    </div>
  );
}
