import React from "react";
import { Link } from "react-router-dom";
import { BsFillCaretDownFill } from "react-icons/bs";
import { isLoggedin, removeUserSession } from "../../../util/comman";
import { useHistory } from "react-router-dom";

export default function Dropdown({ setIslogin }) {
  const history = useHistory();

  const clearSessionstorage = () => {
    removeUserSession();

    history.push("/");
    setIslogin(isLoggedin());
  };

  return (
    <div className="dropdown login-btn mt-3 ">
      <div
        // className="bg-light"
        type="button"
        id="dropdownMenuButton1"
        data-bs-toggle="dropdown"
        aria-expanded="false"
      >
        <BsFillCaretDownFill />
      </div>
      <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
        <li>
          <Link to="/" className="text-dark">
            <p onClick={clearSessionstorage} className="ms-2 mb-0">
              Logout
            </p>
          </Link>
        </li>
      </ul>
    </div>
  );
}
