import React, { useEffect, useState } from "react";
import { FaMapMarkerAlt } from "react-icons/fa";
import "./jobpostcard.scss";
import { getToken } from "../../../util/comman";

import Button from "../Button/Button";
import CustomModal from "../CustomModal/CustomModal";

import axios from "axios";

export default function JobPostCard({
  id,
  title,
  description,
  location,
  btnApplyJob = false,
  bntViewparticipant = false,
}) {
  const baseurl = process.env.REACT_APP_BASE_URL;
  const urlappliedjobs = `${baseurl}candidates/jobs`;

  const [participants, setParticipants] = useState([]);
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    setParticipants([]);
    console.log("participants", participants);
  }, [id]);

  const ApplyJobs = async () => {
    console.log(id);
    await axios
      .post(
        urlappliedjobs,
        {
          jobId: id,
        },
        {
          headers: {
            Authorization: getToken(),
          },
        }
      )
      .then((res) => {
        console.log(res, "RES");
        if (res.status === 201) {
          alert("Job Applied");
        }
      })
      .catch((err) => {
        alert(err.response?.data?.message);
      });
  };

  const viewParticipants = async () => {
    console.log(id);
    const urlviewparticipant = `${baseurl}recruiters/jobs/${id}/candidates`;
    await axios
      .get(urlviewparticipant, {
        headers: {
          Authorization: getToken(),
        },
      })
      .then((res) => {
        console.log(res);
        setParticipants(res.data.data);
        handleShow();
      });
    // alert("View participants");
    // console.log(participants);
  };

  console.log("part", participants);

  return (
    <>
      <div className="bg-light rounded shadow-sm job-card" key={id}>
        <h4>{title}</h4>
        <div className="card-desc">
          <p>{description}</p>
        </div>

        <div className="d-flex justify-content-between">
          <p className="">
            <FaMapMarkerAlt className="text-primary me-1" />
            {location}
          </p>
          {btnApplyJob && (
            <Button name="Apply" btntype="btn-primary" onClick={ApplyJobs} />
          )}

          {bntViewparticipant ? (
            <>
              <Button
                name="View Participant"
                btntype="btn-primary"
                onClick={viewParticipants}
              />
              <CustomModal
                show={show}
                setShow={setShow}
                participants={participants}
              />
            </>
          ) : (
            void 0
          )}
        </div>
      </div>
    </>
  );
}
