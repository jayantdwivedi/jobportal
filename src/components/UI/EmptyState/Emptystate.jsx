import React from "react";
import "./empty.scss";
import { FaRegIdCard } from "react-icons/fa";
import { Link } from "react-router-dom";
export default function Emptystate({ message, rec = false, applied = false }) {
  return (
    <div className="empty-state">
      <div className="display-1 m-4 text-primary text-center d-flex justify-content-center align-items-center">
        <FaRegIdCard className="display-1 opacity-50" />
      </div>

      <div className="empty-content m-4 text-center d-flex justify-content-center align-items-center">
        {message}
      </div>
      <div className="d-flex justify-content-center align-items-center">
        {rec && (
          <Link to="/dashboard/jobpost" className=" btn btn-primary">
            Post a Job
          </Link>
        )}
        {applied && (
          <Link to="/dashboard/appliedjobs" className=" btn-primary">
            Applied Jobs
          </Link>
        )}
      </div>
    </div>
  );
}
