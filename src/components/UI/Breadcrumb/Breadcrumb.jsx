import React from "react";
import { MdHome } from "react-icons/md";
import { Link } from "react-router-dom";
import "./breadcrumb.scss";

export default function Breadcrumb({ next = "", nextCrumb = "false" }) {
  return (
    <div className="my-breadcrumb">
      <div aria-label="breadcrumb" className="ms-4">
        <ol className="breadcrumb">
          <li className="breadcrumb-item">
            <MdHome className="text-light me-2" />
            <Link to="/dashboard" className="text-light text-decoration-none">
              Home
            </Link>
          </li>
          {nextCrumb && (
            <li
              className="breadcrumb-item active text-light"
              aria-current="page"
            >
              {next}
            </li>
          )}
        </ol>
      </div>
    </div>
  );
}
