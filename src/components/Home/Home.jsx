import "./home.scss";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

// Importing Images
import ladyImg from "../../assets/working-lady.jpg";
import liva from "../../assets/liva.png";
import kanba from "../../assets/kanba.png";
import goldline from "../../assets/goldline.png";
import solaytic from "../../assets/solaytic.png";
import velocity9 from "../../assets/velocity9.png";
import ztos from "../../assets/ztos.png";
import hexa from "../../assets/hexa.png";
import amara from "../../assets/amara.png";

export default function Home() {
  const TITLE = "Job Portal - Home";
  return (
    <>
      <Helmet>
        <title>{TITLE}</title>
        <meta name="description" content="Job portal is one of the website that provide different jobs to the user "/>
        <meta name="title" content="job portal home page"/>
      </Helmet>
      <div className="container">
        <section className="row sec-1">
          <div className="col-lg-6 col-md-6 col-sm-12 mt-5">
            <h1 className="text-light">
              Welcome to
              <br />
              My<span>Jobs</span>
            </h1>
            <br />
            <Link
              to="/login"
              className="btn btn-primary btn-link-start px-4 py-2 mb-4"
            >
              Get Started
            </Link>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12">
            <img
              className="img-fluid img-lady"
              src={ladyImg}
              alt="image of working lady"
            />
          </div>
        </section>
        <h3 className="whyus">Why Us</h3>
        <section className="row sec-2">
          <div className="col-lg-3 col-md-3 col-sm-12 card-my">
            <h4>
              <span>
                Get More <br /> Visibility
              </span>
            </h4>
            <p className="">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga
              alias, quos nemo quas harum sequi.
            </p>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-12 card-my">
            <h4>
              <span>
                Organise your <br /> candidate
              </span>
            </h4>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga
              alias, quos nemo quas harum sequi.{" "}
            </p>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-12 card-my">
            <h4>
              <span>
                Verify their <br /> abilities
              </span>
            </h4>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga
              alias, quos nemo quas harum sequi.{" "}
            </p>
          </div>
        </section>
        <h3>Companies Who Trusted Us</h3>
        <section className="images">
          <img className="img" src={solaytic} alt="solaytic" />
          <img className="img" src={liva} alt="liva" />
          <img className="img" src={liva} alt="ideaa" />
          <img className="img" src={kanba} alt="kanba" />
          <img className="img" src={goldline} alt="goldline" />
          <img className="img" src={solaytic} alt="solaytic" />
          {/* </section>
        <section className="images"> */}
          <img className="img" src={velocity9} alt="velocity9" />
          <img className="img" src={hexa} alt="hexa" />
          <img className="img" src={ztos} alt="ztos" />
          <img className="img" src={amara} alt="amara" />
          <img className="img" src={goldline} alt="goldline" />
          <img className="img" src={solaytic} alt="solaytic" />
        </section>
      </div>
    </>
  );
}
