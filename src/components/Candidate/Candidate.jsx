import React, { useState, useEffect } from "react";
import "./Style.scss"

import JobPostCard from "../../components/UI/JobPostCard/JobPostCard";
import Pagination from "../UI/Pagination/Pagination";
import { Helmet } from "react-helmet";
import { Spinner } from "react-bootstrap";

export default function Candidate() {
  const TITLE = "Job Portal - Dashboard | Candidate";
  const [jobs, setJobs] = useState([]);
  const [loading,setloading] = useState(true);

  const baseurl = process.env.REACT_APP_BASE_URL;
  const url = `${baseurl}/jobs`;

  useEffect(() => {
    fetchData();
    console.log("Use effect callled ");
  }, []);

  const fetchData = async () => {
    const res = await fetch(url);
    const jsonres = await res.json();
    setJobs(jsonres.data);
    setloading(false);
    console.log("Jobs", jobs);
  };

  // Pagination
  const [pageNumber, setPageNumber] = useState(0);
  const jobsPerPage = 6;
  const pagesVisited = pageNumber * jobsPerPage;

  const pageCount = Math.ceil(jobs.length / jobsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  const displayJobs = jobs
    .slice(pagesVisited, pagesVisited + jobsPerPage)
    .map(function (element) {
      return (
        <JobPostCard
          id={element.id}
          title={element.title}
          description={element.description}
          location={element.location}
          btnApplyJob={true}
        />
      );
    });

  return (
    <>
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      {
        loading ? (
         <>
         <div className="d-flex justify-content-center align-items-center spinner-body">
          <Spinner animation="grow" variant="danger" />
          <Spinner animation="grow" variant="info" />
          <Spinner animation="grow" variant="warning" />
          <Spinner animation="grow" variant="dark" />
      </div>
        </>
        ):(
          <>
          <h4 className="text-light job-for-u">Jobs for you</h4>
           <div className="mb-4">
          <div className="d-flex flex-wrap">{displayJobs}</div>
          <div className="d-flex justify-content-center space-bottom">
          <Pagination pageCount={pageCount} onPageChange={changePage} />
          </div>
          </div>
          </>
        )
      }
    </>
  );
}
