import React from "react";
import { Redirect, Route } from "react-router-dom";
import Home from "../components/Home/Home";
import Dashboard from "../screens/dashboard/Dashboard";
import { isLoggedin } from "../util/comman";

export const ProtectedRoute = ({ component: Component, ...rest }) => {
  if (!isLoggedin()) {
    return <Redirect to="/login" />;
  }

  //   if (!isLoggedin) {
  //     return <Redirect to="/dashboard" component={Dashboard} />;
  //   }
  return <Route {...rest} component={Component} />;
};
