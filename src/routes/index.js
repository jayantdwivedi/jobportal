import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";
// import Background from "./components/Background/Background";
// import Navbar from "./components/Navbar/Navbar";
import Home from "../components/Home/Home";
import Login from "../screens/login/Login";
import ResetPass from "../screens/resetPass/ResetPass";
import Forgotpass from "../screens/forgotpass/Forgotpass";
import Dashboard from "../screens/dashboard/Dashboard";
import Signup from "../screens/signup/Signup";
import Applied from "../screens/Applied/Applied";
import PostJob from "../screens/PostJob/PostJob";
import { ProtectedRoute } from "./customRoute";
import Navbar from "../components/Navbar/Navbar";
import Background from "../components/Background/Background";
import PagenotFound from "../screens/Pagenotfound/PagenotFound";

const Routes = () => {
  const [islogin, setIslogin] = useState(false);
  //   console.log("Is Logggged in", isLoggedin());

  return (
    <>
      <Navbar isLogin={islogin} setIslogin={setIslogin} />
      <Background />
      <Switch>
        <ProtectedRoute path="/dashboard/jobpost" exact component={PostJob} />
        <ProtectedRoute path="/dashboard" exact component={Dashboard} />
        <ProtectedRoute
          path="/dashboard/appliedjobs"
          exact
          component={Applied}
        />
        <Route
          path="/login"
          exact
          component={() => <Login setIslogin={setIslogin} />}
        />

        <Route path="/forgot-pass/reset-pass" exact component={ResetPass} />
        <Route path="/forgot-pass" exact component={Forgotpass} />

        <Route
          path="/signup"
          exact
          component={() => <Signup setIslogin={setIslogin} />}
        />
        <Route path="/" exact component={Home} />
        <Route
          path="*"
          component={PagenotFound}
        />
      </Switch>
    </>
  );
};

export default Routes;
