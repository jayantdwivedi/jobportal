// return the token from the session storage
export const getToken = () => {
  return sessionStorage.getItem("token") || null;
};

// remove the token and user from the session storage
export const removeUserSession = () => {
  sessionStorage.removeItem("token");
  sessionStorage.removeItem("userRole");
  sessionStorage.removeItem("username");
};

// set the token and user from the session storage
export const setUserSession = (token) => {
  sessionStorage.setItem("token", token);
};

// set user Role
export const setUserRole = (userRole) => {
  sessionStorage.setItem("userRole", userRole);
};

// return the userRole to dashboard
export const getUserRole = () => {
  return sessionStorage.getItem("userRole");
};

// Set and return username
export const setUsername = (name) => {
  sessionStorage.setItem("username", name);
};

export const getUsername = () => {
  return sessionStorage.getItem("username");
};

// if the candidate login
export const isLoggedin = () => {
  if (sessionStorage.getItem("token") != null) {
    return true;
  } else {
    return false;
  }
};

// Regex to check email
export const emailRegex = /\S+@\S+\.\S+/;
