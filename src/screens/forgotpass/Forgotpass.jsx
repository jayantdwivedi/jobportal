import React, { useState } from "react";
import "../../styles/form.scss";
import { useHistory } from "react-router";
import { emailRegex, setUserSession, getToken } from "../../util/comman";

import Button from "../../components/UI/Button/Button";
import Input from "../../components/UI/Input/Input";
import Error from "../../components/UI/ErrorMsg/Error";
import axios from "axios";
import { Helmet } from "react-helmet";

export default function Forgotpass() {
  const TITLE = "Job Portal - Forgot Password";

  const history = useHistory();
  const [emailAdd, setEmail] = useState("");
  const [error, setError] = useState();
  const [token, setToken] = useState();
  const [err, setErr] = useState();

  const baseUrl = process.env.REACT_APP_BASE_URL;
  const url = `${baseUrl}auth/resetpassword?email=${emailAdd}`;
  const verifyTokenUrl = `${baseUrl}/auth/resetpassword/${token}`;

  const emailCheck = emailRegex;

  const handleSubmit = async () => {
    if (validate(emailAdd)) {
      await axios
        .get(url)
        .then((response) => {
          console.log(response);
          setUserSession(response.data.data.token);
          if (response.data.code === 201) {
            setToken(getToken());
            if (verifyToken()) {
              history.push({ pathname: "/forgot-pass/reset-pass" });
              // console.log("token valid");
            }
          }
          if (response.data.code === 404) {
            console.log("Else portion");
            setError("Something went wrong");
          }
        })
        .catch((err) => {
          console.log("error->", err);
          // setErr(err);
          setError(err.response?.data?.message);
        });
    }
  };

  console.log("Error state ");

  const verifyToken = async () => {
    if (token != null) {
      await axios
        .get(verifyTokenUrl)
        .then((res) => {
          console.log(res.data.code);
          if (res.data.data == 200) {
            return true;
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    return false;
  };

  const validate = (emailAdd) => {
    if (emailAdd == undefined) {
      setError("");
      setError("Field is mandatory");
    } else if (!emailCheck.test(emailAdd)) {
      setError("");
      setError("Enter a valid email");
    } else {
      return true;
    }
  };

  return (
    <>
      <Helmet>
        <title>{TITLE}</title>
        <meta name="tag" content="job portal forgot password"/>
        <meta name="description" content="Job portal forgot password section provide service to add a new password"/>
      </Helmet>
      <div className="main-body">
        <h3>Forgot Password?</h3>
        <p>
          Enter the email associated with your account and we'll send you
          instruction to reset your password
        </p>
        <Input
          labelText="Email address"
          value={emailAdd}
          type="email"
          placeholder="Enter your email"
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        />
        <Error errmsg={error} />
        <Button name="Submit" onClick={handleSubmit} />
      </div>
    </>
  );
}
