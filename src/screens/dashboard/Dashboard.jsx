import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";

import "./dashboard.scss";

import { getUserRole } from "../../util/comman";

import Breadcrumb from "../../components/UI/Breadcrumb/Breadcrumb";
import Recruiter from "../../components/Recruiter/Recruiter";
import Candidate from "../../components/Candidate/Candidate";

export default function Dashboard() {
  const TITLE = "Job Portal - Dashboard | Recruiter";
  return (
    <>
      <Helmet>
        <title>{TITLE}</title>
        <meta name="tag" content="Job portal dashboard"/>
        <meta name="description" content="Job portal provide dashboard where you can find the jobs and post the as well"/>
      </Helmet>
      <Breadcrumb />
      <div className="main-dash">
        {getUserRole() == 1 ? <Candidate /> : <Recruiter />}
      </div>
    </>
  );
}
