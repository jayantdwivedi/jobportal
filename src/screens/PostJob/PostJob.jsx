import React, { useState } from "react";
import "../../styles/form.scss";
import "./postjob.scss";
import axios from "axios";

import Button from "../../components/UI/Button/Button";
import Input from "../../components/UI/Input/Input";
import Error from "../../components/UI/ErrorMsg/Error";
import Breadcrumb from "../../components/UI/Breadcrumb/Breadcrumb";

import { getToken } from "../../util/comman";

export default function PostJob() {
  const [jobTitle, setJobTitle] = useState();
  const [desc, setDesc] = useState();
  const [location, setLocation] = useState();

  const [error, setError] = useState();
  const [errorTitle, setErrorTitle] = useState();
  const [errorDesc, setErrorDesc] = useState();
  const [errorLocation, setErrorLocation] = useState();

  const baseurl = process.env.REACT_APP_BASE_URL;
  const url = `${baseurl}/jobs`;

  const handleSubmit = async () => {
    if (validate(jobTitle, desc, location)) {
      await axios
        .post(
          url,
          {
            title: jobTitle,
            description: desc,
            location: location,
          },
          {
            headers: {
              Authorization: getToken(),
            },
          }
        )
        .then((res) => {
          console.log(res);
          setJobTitle("");
          setDesc("");
          setLocation("");
          setErrorTitle("");
          setErrorLocation("");
          setErrorDesc("");
          alert("Job posted");
        })
        .catch((err) => {
          console.log(err.response?.data?.errors);
          if (err.response?.data?.errors.length > 0) {
            // setErrorTitle("h");
          }
        });
    }
  };

  function validate(jobTitle, desc, location) {
    if (
      jobTitle === "" ||
      jobTitle === undefined ||
      desc === "" ||
      desc === undefined ||
      location === "" ||
      location === undefined
    ) {
      setError("All feild is mandatory");
    }
    if (!(jobTitle?.length > 3 && jobTitle?.length < 100)) {
      setErrorTitle("Job Title must be between 3 and 100 characters");
    }
    if (jobTitle?.length > 3 && jobTitle?.length < 100) {
      setErrorTitle("");
    }
    if (!(desc?.length > 3 && desc?.length < 100)) {
      setErrorDesc("Job Description must be between 3 and 100 characters");
    }
    if (desc?.length > 3 && desc?.length < 100) {
      setErrorDesc("");
    }

    if (!(location?.length > 3 && location?.length < 100)) {
      setErrorLocation("Job Location must be between 3 and 100 characters");
    }
    if (location?.length > 3 && location?.length < 100) {
      setErrorLocation("");
    }
    return true;
  }

  return (
    <>
      <Breadcrumb next="Post Job" nextCrumb={true} />
      <div className="main-body">
        <h3>Post a Job</h3>
        <Input
          labelText="Job Title"
          value={jobTitle}
          placeholder="Enter job title"
          onChange={(e) => {
            setJobTitle(e.target.value);
          }}
        />
        <Error errmsg={errorTitle} />
        <label>
          Job Description<sup>*</sup>
        </label>
        <br />
        <textarea
          type="text"
          className="input-box p-2 my-3 rounded"
          rows="5"
          value={desc}
          placeholder="Enter job description"
          onChange={(e) => {
            setDesc(e.target.value);
          }}
        />
        <Error errmsg={errorDesc} />
        <Input
          labelText="Job Location"
          value={location}
          placeholder="Enter job lcoation"
          onChange={(e) => {
            setLocation(e.target.value);
          }}
        />
        <Error errmsg={errorLocation} />
        <Button onClick={handleSubmit} name="Post" />
      </div>
    </>
  );
}
