import React from "react";
import {useHistory} from "react-router";
import Img from "./404.jpg"


export default function PagenotFound() {
    const history = useHistory();
    return (
        <div className='main-body img-bg'>
            <img src={Img} alt="" className="img-fluid opacity-80"  />

            <div className="d-flex justify-content-center align-items-center">
                <div className='btn btn-primary d-inline' onClick={() => history.push('/')}>
                     <button className="btn btn-primary">Home Page</button>
                </div>
            </div>
            
        </div>
    )
}