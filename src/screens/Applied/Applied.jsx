import axios from "axios";
import React, { useEffect, useState } from "react";

import { getToken } from "../../util/comman";
// import "../dashboard/dasboard.scss";

import Breadcrumb from "../../components/UI/Breadcrumb/Breadcrumb";
import Emptystate from "../../components/UI/EmptyState/Emptystate";
import Pagination from "../../components/UI/Pagination/Pagination";
import JobPostCard from "../../components/UI/JobPostCard/JobPostCard";

export default function Applied() {
  const [jobs, setJobs] = useState([]);
  const baseurl = process.env.REACT_APP_BASE_URL;
  const url = `${baseurl}/candidates/jobs/applied`;

  useEffect(() => {
    axios
      .get(url, {
        headers: {
          Authorization: getToken(),
        },
      })
      .then((res) => {
        console.log(res.data.data);
        setJobs(res.data.data);
      });
  }, []);

  // Pagination
  const [pageNumber, setPageNumber] = useState(0);
  const jobsPerPage = 6;
  const pagesVisited = pageNumber * jobsPerPage;

  const pageCount = Math.ceil(jobs.length / jobsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  const displayJobs = jobs
    .slice(pagesVisited, pagesVisited + jobsPerPage)
    .map(function (element) {
      return (
        <JobPostCard
          id={element.id}
          title={element.title}
          description={element.description}
          location={element.location}
          btn={false}
        />
      );
    });

  return (
    <>
      <Breadcrumb nextCrumb={true} next="Applied" />
      <h4 className="text-light ms-4 page-heading">Jobs Applied by you</h4>
      <div className="main-dash">
        {/* <h1>Applied jobs</h1> */}
        <div>
          {jobs.length !== 0 ? (
            <>
              <div className="d-flex flex-wrap">{displayJobs}</div>
              <div className="d-flex justify-content-center space-bottom">
                <Pagination pageCount={pageCount} onPageChange={changePage} />
              </div>
            </>
          ) : (
            <Emptystate
              message={"Your applied jobs are show here"}
              applied={true}
            />
          )}
        </div>
        {/* <Emptystate /> */}
      </div>
    </>
  );
}
