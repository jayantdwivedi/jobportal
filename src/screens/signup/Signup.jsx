import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import "../../styles/form.scss";
import { Helmet } from "react-helmet";

import { FaSearch } from "react-icons/fa";
import { MdGroup } from "react-icons/md";
import {
  setUserSession,
  emailRegex,
  setUserRole,
  setUsername,
  isLoggedin,
} from "../../util/comman";

import Button from "../../components/UI/Button/Button";
import Error from "../../components/UI/ErrorMsg/Error";
import Input from "../../components/UI/Input/Input";

export default function Signup({ setIslogin }) {
  const TITLE = "Job Portal - Signup";
  const history = useHistory();

  const baseUrl = process.env.REACT_APP_BASE_URL;

  const url = `${baseUrl}auth/register`;

  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [pass, setPass] = useState();
  const [confirmpass, setConfirmpass] = useState();
  const [skills, setSkills] = useState();
  const [userrole, setUserrole] = useState(0);

  const [error, setError] = useState();
  const [emailErr, setEmailErr] = useState();
  const [passerr, setPassErr] = useState();
  const [errRole, setErrRole] = useState();

  const emailCheck = emailRegex;
  const handleSubmit = async () => {
    if (validation(name, email, pass, confirmpass, skills)) {
      await axios
        .post(url, {
          email: email,
          userRole: userrole,
          password: pass,
          confirmPassword: confirmpass,
          name: name,
          skills: skills,
        })
        .then((response) => {
          console.log(response);
          setError(response.message);
          setUserSession(response.data.data.token);
          setUserRole(response.data.data.userRole);
          setUsername(response.data.data.name);
          setIslogin(isLoggedin());

          history.push("/dashboard");
          setConfirmpass("");
          setPass("");
          setEmail("");
          setName("");
          setSkills("");
          setError("");
          setEmailErr("");
          setPassErr("");
          setErrRole("");
        })
        .catch((err) => {
          // console.log(err.response?.data?.errors[0]?.name, "Error login");
          if (err.response?.data?.errors) {
            err.response?.data?.errors.map((ele) => {
              setError(ele.name);
            });
          } else {
            setError(err.response?.data?.message);
          }

          // console.log(err.response?.data?.errors.length, "Errors ");

          // setError(err.response?.data?.errors);
          if (err.code == 422) {
            setError(err.message);
          }
          console.log(err.code);
        });
      // history.push({ pathname: "/login" });
      console.log(userrole);
      console.log(name);
      console.log(email);
      console.log(pass);
      console.log(confirmpass);
      console.log(skills);
    }
  };

  const validation = (name, email, pass, confirmpass, skills) => {
    if (
      name === undefined ||
      name === "" ||
      email === undefined ||
      email === "" ||
      pass === undefined ||
      pass === "" ||
      confirmpass === undefined ||
      confirmpass === "" ||
      skills === undefined ||
      skills === ""
    ) {
      setError("All feild are mandatory");
    } else if (pass !== confirmpass) {
      setPassErr("Both password must be same");
    } else if (!emailCheck.test(email)) {
      setEmailErr("Invalid email");
    } else if (pass.length < 6) {
      setPassErr("Password must be more than 6 character");
    } else if (userrole == undefined) {
      setErrRole("Please choose a role");
    } else {
      return true;
    }
  };

  const userRoleRecruiter = () => {
    setUserrole(0);
  };

  const userRoleCandidate = () => {
    setUserrole(1);
  };

  return (
    <>
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      <div className="main-body">
        <h3>SignUp</h3>
        <p>
          I am a<sup>*</sup>
        </p>
        <div className="d-flex gap-3">
          <Button
            name="Recruiter"
            btntype={userrole ? "btn-light" : "btn-primary"}
            onClick={userRoleRecruiter}
            icon={<FaSearch className="mx-2 my-2 fs-6" />}
          />

          <Button
            name="Candidate"
            btntype={userrole ? "btn-primary" : "btn-light"}
            onClick={userRoleCandidate}
            icon={<MdGroup className="mx-2 my-2 fs-6" />}
          />
        </div>
        <Error errmsg={errRole} />

        <Input
          labelText="Full Name"
          type="text"
          value={name}
          placeholder="Enter your name"
          onChange={(e) => {
            setName(e.target.value);
          }}
        />

        <Input
          labelText="Email"
          type="email"
          value={email}
          placeholder="Enter your email"
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        />
        <Error errmsg={emailErr} />
        <div className="row">
          <div className="col-lg-6">
            <Input
              labelText="Password"
              type="password"
              value={pass}
              placeholder="Enter your password"
              onChange={(e) => {
                setPass(e.target.value);
              }}
            />
          </div>
          <div className="col-lg-6">
            <Input
              labelText="Confirm Password"
              type="password"
              value={confirmpass}
              placeholder="Enter your password"
              onChange={(e) => {
                setConfirmpass(e.target.value);
              }}
            />
            <Error errmsg={passerr} />
          </div>
        </div>

        <Input
          labelText="Skills"
          type="text"
          value={skills}
          placeholder="Enter your skills"
          onChange={(e) => {
            setSkills(e.target.value);
          }}
        />
        <Error errmsg={error} />

        <div className="form-end-div">
          <p className="new-job">New to jobs? </p>
          <Link to="/login" className="link-form create-acc px-1">
            Login
          </Link>
        </div>

        <Button name="Signup" onClick={handleSubmit} />
      </div>
    </>
  );
}
