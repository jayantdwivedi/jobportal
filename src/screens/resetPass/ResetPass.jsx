import React, { useState } from "react";
import "../../styles/form.scss";
import { getToken } from "../../util/comman";
import { useHistory } from "react-router";

import Input from "../../components/UI/Input/Input";
import Button from "../../components/UI/Button/Button";
import Error from "../../components/UI/ErrorMsg/Error";
import axios from "axios";

export default function ResetPass() {
  const history = useHistory();
  const [newpass, setNewpass] = useState();
  const [confirmpass, setConfirmpass] = useState();
  const [error, setError] = useState();
  const [token, setToken] = useState();

  const baseUrl = process.env.REACT_APP_BASE_URL;
  const url = `${baseUrl}/auth/resetpassword`;

  const handleSubmit = async () => {
    setToken(getToken());
    if (validate(newpass, confirmpass)) {
      await axios
        .post(url, {
          password: newpass,
          confirmPassword: confirmpass,
          token: token,
        })
        .then((res) => {
          setError("");
          setConfirmpass("");
          setNewpass("");
          alert(res.data.message);
          history.push({ pathname: "/login" });
        })
        .catch((err) => {
          setError(err.response?.data?.message);
        });
    }
  };

  const validate = (newpass, confirmpass) => {
    if (
      newpass == undefined ||
      confirmpass == undefined ||
      newpass == "" ||
      confirmpass == ""
    ) {
      setError("");
      setError("All field is mandatory");
    } else if (newpass.length < 6) {
      setError("Password must be more than 6 characters");
    } else if (newpass !== confirmpass) {
      setError("");
      setError("Enter same password in both");
    } else {
      return true;
    }
  };

  return (
    <>
      <div className="main-body">
        <h3>Reset Password</h3>
        <p>Enter your password below</p>
        <Input
          labelText="New Password"
          type="password"
          value={newpass}
          placeholder="Enter your password"
          onChange={(e) => {
            setNewpass(e.target.value);
          }}
        />
        <Input
          labelText="Confirm new password"
          type="password"
          value={confirmpass}
          placeholder="Enter your confirm password"
          onChange={(e) => {
            setConfirmpass(e.target.value);
          }}
        />
        <Error errmsg={error} />
        <Button onClick={handleSubmit} name="Reset" />
      </div>
    </>
  );
}
