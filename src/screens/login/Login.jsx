import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import "../../styles/form.scss";
import { Helmet } from "react-helmet";

import {
  setUserSession,
  emailRegex,
  setUserRole,
  setUsername,
  isLoggedin,
} from "../../util/comman";
import Button from "../../components/UI/Button/Button";
import Input from "../../components/UI/Input/Input";
import Error from "../../components/UI/ErrorMsg/Error";

export default function Login({ setIslogin }) {
  const TITLE = "Job Portal - Login";
  const history = useHistory();

  const baseUrl = process.env.REACT_APP_BASE_URL;

  const url = `${baseUrl}auth/login/`;

  const [email, setEmail] = useState();
  const [pass, setPass] = useState();
  const [error, setError] = useState();
  const [errorEmail, setErrorEmail] = useState();

  const handleSubmit = async () => {
    if (validate(email, pass)) {
      await axios
        .post(url, { email: email, password: pass })
        .then((response) => {
          console.log(response.data);
          console.log(response, "REsponse login");
          console.log("Sucess");

          setEmail("");
          setPass("");
          setError("");

          setErrorEmail("");
          setUserSession(response.data.data.token);
          setUserRole(response.data.data.userRole);
          setUsername(response.data.data.name);
          // console.log("token " + response.data.data.token);

          history.push("/dashboard");
          setIslogin(isLoggedin());
        })
        .catch((err) => {
          // setError("Something went wrong!");
          // console.log(err.response.data.message, "Error");
          setError(err.response?.data?.message);
          // setError(err);
        });
    }

    // console.log(baseUrl);
  };

  const emailCheck = emailRegex;

  function validate(email, pass) {
    if (
      email === "" ||
      email === undefined ||
      pass === "" ||
      pass === undefined
    ) {
      setError("All field is mandatory");
    } else if (!emailCheck.test(email)) {
      setErrorEmail("Enter a valid email");
    } else {
      return true;
    }
  }

  return (
    <>
      <Helmet>
        <title>{TITLE}</title>
        <meta name="description" content="Job portal website provide the place to find the jobs without difficulty  "/>
        <meta name="title" content="job portal login page"/>
      </Helmet>
      <div className="main-body">
        <h3>Login</h3>
        <Input
          labelText="Email Address"
          value={email}
          placeholder="Enter email"
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        />
        <Error errmsg={errorEmail} />

        <div>
          <label htmlFor="Password" className="d-inline-block">
            Password <sup>*</sup>
          </label>
          <Link
            to="/forgot-pass"
            className="d-inline-block link-form create-acc float-right"
          >
            Forgot password
          </Link>
        </div>

        <Input
          value={pass}
          type="password"
          placeholder="Enter Password"
          onChange={(e) => {
            setPass(e.target.value);
          }}
        />
        <Error errmsg={error} />

        <Button onClick={handleSubmit} name="Login" />

        <div className="form-end-div">
          <p className="new-job">New to jobs? </p>
          <Link to="/signup" className="link-form create-acc px-1">
            Create an account
          </Link>
        </div>
      </div>
    </>
  );
}
